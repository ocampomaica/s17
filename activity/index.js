console.log("Hello World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

let yourName = prompt('Enter your name: ');
console.log('Name: ' + yourName);

let yourAge = prompt('Enter your age: ');
console.log('Age: ' + yourAge);

let yourLocation = prompt('Enter your location: ');
console.log('Location: ' + yourLocation);

alert("Thank you for your input!");

console.log("")



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function getBand(){
	const faveBand = ['Maroon 5', 'Paramore', 'Goo goo Dolls', 'Incubus','Snow Patrol'];

	for (let x=0; x < faveBand.length; x++) {
		console.log(x+1+". " + faveBand[x]);
	}
	console.log(faveBand);
};
console.log("Here are my favorite Bands:")
getBand();

console.log("")



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
function getMovies(){
	const faveMovie = [
	'Sea Beast - 87%', 
	'The Mitchells VS. The Machines - 89%', 
	'Wish Dragon - 79%', 
	'Monster House - 63%',
	'Klaus - 96%'
	]

	for (let x=0; x < faveMovie.length; x++) {
		console.log(x+1+". " + faveMovie[x]);
	}
	console.log(faveMovie);
};
console.log("Here are my favorite Movies:")
getMovies();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
/*console.log(friend1);
console.log(friend2);*/